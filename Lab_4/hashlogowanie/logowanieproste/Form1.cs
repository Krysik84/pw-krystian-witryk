﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace logowanieproste
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2();
            f.login1 = login.Text;
            f.password1 = password.Text;
            if ((f.login1 == "krysik84") && (CreateMD5(f.password1) == CreateMD5("321")))
            {
                Napis.Text = "";
            
                f.Show();
            }
            else if ((f.login1 == "adi") && (CreateMD5(f.password1) == CreateMD5("321")))
            {
                f.Show();
                Napis.Text = "";
            }
            else
            {
                Napis.ForeColor = Color.Red;
                Napis.Text = "Błędne dane logowania";
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
